package org.arcane418.quarkus.systemctl.deployment;

import io.quarkus.arc.deployment.AdditionalBeanBuildItem;
import io.quarkus.arc.deployment.BeanContainerBuildItem;
import io.quarkus.deployment.annotations.BuildProducer;
import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.annotations.ExecutionTime;
import io.quarkus.deployment.annotations.Record;
import io.quarkus.deployment.builditem.FeatureBuildItem;
import io.quarkus.deployment.builditem.ShutdownContextBuildItem;
import io.quarkus.deployment.builditem.nativeimage.ReflectiveClassBuildItem;
import org.arcane418.quarkus.systemctl.runtime.SystemctlStartupEvent;
import org.arcane418.quarkus.systemctl.runtime.SystemctlStartupService;
import org.arcane418.quarkus.systemctl.runtime.SystemdNotifyRecorder;


class SystemdNotifyProcessor {
    private static final String FEATURE = "systemd-event-notify";


    @BuildStep
    void build(BuildProducer<AdditionalBeanBuildItem> additionalBeanProducer,
               BuildProducer<FeatureBuildItem> featureProducer) {

        featureProducer.produce(new FeatureBuildItem(FEATURE));

        AdditionalBeanBuildItem beanBuilItem = AdditionalBeanBuildItem.unremovableOf(SystemctlStartupService.class);
        additionalBeanProducer.produce(beanBuilItem);

    }

    @BuildStep
    void reflection(BuildProducer<ReflectiveClassBuildItem> reflectiveClasses) {
        reflectiveClasses.produce(new ReflectiveClassBuildItem(true, true,
                SystemctlStartupEvent.class.getCanonicalName()));
        reflectiveClasses.produce(new ReflectiveClassBuildItem(true, true,
                SystemdNotifyRecorder.class.getCanonicalName()));

    }


    @BuildStep
    @Record(ExecutionTime.RUNTIME_INIT)
    void onQuarkusStarted(SystemdNotifyRecorder recorder, ShutdownContextBuildItem shutdownContextBuildItem ,
                          BeanContainerBuildItem beanContainer) {
        recorder.startRuntimeService(shutdownContextBuildItem, beanContainer.getValue());
    }
}