package org.arcane418.quarkus.systemctl.runtime;

import io.quarkus.arc.runtime.BeanContainer;
import io.quarkus.runtime.ShutdownContext;
import io.quarkus.runtime.annotations.Recorder;
@Recorder
public class SystemdNotifyRecorder {

    public void startRuntimeService(ShutdownContext shutdownContext, BeanContainer container) {
        SystemctlStartupService service = container.beanInstance(SystemctlStartupService.class);
        service.addShutdownContext(shutdownContext);
    }


}
