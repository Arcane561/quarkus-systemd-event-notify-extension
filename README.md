# Quarkus Systemd Event Notify Extension



## Introduction

This extension is used to notify Linux service manager (systemd) about start-up completion and other service status changes.

## Usage

To use the extension, add the dependency to the target project:

Maven:
```
<dependency>
    <groupId>org.arcane418.quarkus.systemctl</groupId>
    <artifactId>quarkus-systemd-notify</artifactId>
    <version>${quarkus.systemd.notify.version}</version>
</dependency>
```
Gradle:
```
implementation 'org.arcane418.quarkus.systemctl:quarkus-systemd-notify:${quarkus.systemd.notify.version}'

```

and configure the service unit file with the following minumum configurations:

If you are running from a root user then:

```
...

[Service]
Type=notify

...
```
If the user is a regular user then

```
...

[Service]
Type=notify
AmbientCapabilities=CAP_SYS_ADMIN
...
```

After that you need to inject Event<SystemctlStartupEvent> , the event is located in
jakarta.enterprise.event.Event;

Example

```

import jakarta.enterprise.event.Event;

private final Event<SystemctlStartupEvent> event;

```

You can then call the `fire` method of the `Event` to start the event to notify systemd or call the `fireAsync` method for asynchronous execution


## Systemd Service Example

Assuming `quarkus-run.jar` is located at `/opt/quarkus-app/quarkus-run.jar`:

- Create a unit configuration file at `/etc/systemd/system/quarkus.service`:

```
[Unit]
Description=Quarkus Server
After=network.target
Wants=network.target

[Service]
Type=notify
AmbientCapabilities=CAP_SYS_ADMIN
ExecStart=/bin/java -jar /opt/quarkus-app/quarkus-run.jar
SuccessExitStatus=0 143

[Install]
WantedBy=multi-user.target
```
- Enable the service (this will make it to run at system start-up as well):

```
sudo systemctl enable quarkus
```

- Start/Stop/Restart the service:

```
sudo systemctl start quarkus
sudo systemctl stop quarkus
sudo systemctl restart quarkus
```

- Check status of the service:

```
sudo systemctl status quarkus
```

## systemd-notify with SELinux

If SELinux (Mostly for RedHat OS) is enabled:

```
root# getenforce
Enforcing
```

Add `systemd_notify_t` to the permissive types:

```
semanage permissive -a systemd_notify_t
```

You can check with:

```
semanage permissive -l
```
## Based on `https://github.com/quarkiverse/quarkus-systemd-notify/`